defmodule BoardGameServer.Dino do
  alias BoardGameServer.{Coordinate, Dino}

  @enforce_keys [:coordinate, :state]
  defstruct [:coordinate, :state]

  def new(%Coordinate{} = coordinate), do:
    {:ok, %Dino{coordinate: coordinate, state: :alive}}

end