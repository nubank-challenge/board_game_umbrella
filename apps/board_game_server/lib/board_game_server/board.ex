defmodule BoardGameServer.Board do
  alias BoardGameServer.{Board, Dino, Robot}

  defstruct dinos: %MapSet{}, robot: nil

  def new(), do: %Board{}

  def position_robot(%Board{} = board, %Robot{} = robot) do
    if overlaid?(board, robot.coordinate) do
      {:error, :busy_coordinate}
    else
      {:ok, %{board | robot: robot}}
    end
  end

  def position_dino(%Board{} = board, %Dino{} = dino) do
    if overlaid?(board, dino.coordinate) do
      {:error, :busy_coordinate}
    else
      {:ok, %{board | dinos: MapSet.put(board.dinos, dino)}}
    end
  end

  def overlaid?(%Board{robot: nil, dinos: dinos}, new_coordinate) do
    dinos
    |> Enum.map(&(&1.coordinate))
    |> Enum.member?(new_coordinate)
  end

  def overlaid?(%Board{robot: robot, dinos: dinos}, new_coordinate) do
    [robot, dinos]
    |> List.flatten()
    |> Enum.map(&(&1.coordinate))
    |> Enum.member?(new_coordinate)
  end
end