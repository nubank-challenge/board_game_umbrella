defmodule BoardGameServer.Robot do
  alias BoardGameServer.{Coordinate, Robot}

  @enforce_keys [:coordinate, :face_direction]
  defstruct [:coordinate, :face_direction]

  @directions [:up, :down, :left, :right]

  def new(%Coordinate{} = coordinate, face_direction) when face_direction in(@directions), do:
    {:ok, %Robot{coordinate: coordinate, face_direction: face_direction}}

  def new(%Coordinate{}, _face_direction), do:
    {:error, :invalid_face_direction}
  
end