defmodule BoardGameServer do
  @moduledoc """
  Documentation for BoardGameServer.
  """

  @doc """
  Hello world.

  ## Examples

      iex> BoardGameServer.hello()
      :world

  """
  def hello do
    :world
  end
end
